
let trainer = {
	name : "Ash Ketchum",
	age : 10,
	pokemon : ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {hoenn: ["May", "Max"], kanto: ["Brock", "Misty"]},
	talk: function(){
		console.log("Pikachu! I CHOOSE YOU!!!")
	}
};
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk();


function Pokemon(name, level) {
    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;
}

let pikachu = new Pokemon('Pikachu', 12); 
let geodude = new Pokemon('Geodude', 8);
let mewtwo = new Pokemon('Mewtwo', 100);

console.log(pikachu); 
console.log(geodude);
console.log(mewtwo);

geodude.tackle = function(pikachu){
    pikachu.health -= geodude.attack;
    console.log(geodude.name + ' tackled ' + pikachu.name);
    console.log(pikachu.name + "'s health is now reduced to " + pikachu.health);
    if (pikachu.health <= 0){
        console.log(pikachu.name + ' fainted.');
    }
};

mewtwo.tackle = function(geodude) {
    geodude.health -= mewtwo.attack;
    console.log(mewtwo.name + ' tackled ' + geodude.name);
    console.log(geodude.name + "'s health is now reduced to " + geodude.health);
    if (geodude.health <= 0){
        console.log(geodude.name + ' fainted.');
    }
};

geodude.faint = function(){
    console.log(geodude.name + ' fainted.');
};

geodude.tackle(pikachu);
geodude.tackle(pikachu);
mewtwo.tackle(geodude);
mewtwo.tackle(geodude);
geodude.tackle(pikachu);
mewtwo.tackle(geodude);




// Creates new instances of the "Pokemon" object each with their unique properties
// let pikachu = new Pokemon("Pikachu", 16);
// let rattata = new Pokemon('Rattata', 8);

// // Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
// pikachu.tackle(rattata);